use clap::{Parser, Subcommand};
pub mod secrets;
use secrets::{Secret, SecretID};
use std::process;


#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct App {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Create {
        #[arg(short, long)]
        secret: String,
    
        #[arg(short, long, default_value_t=1)]
        views: u8,
    },
    Lookup {
        #[arg(short, long)]
        id: String,

        #[arg(short, long)]
        password: String,
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>{
    let app = App::parse();

    match &app.command {
        Some(Commands::Create { secret, views }) => {
            let secret = Secret{ text: secret.to_string(), views: *views};
            let res = secret.create().await.unwrap_or_else(|e| {
                println!("error {}", e);
                process::exit(1);
            });

            println!("ID is {}\nPassword is {}", res.id, res.password);
        }
        Some(Commands::Lookup { id, password }) => {
            let secret_id = SecretID{ id: id.to_string(), password: password.to_string()};
            let secret = secret_id.lookup().await.unwrap_or_else( |e| {
                println!("error {}", e);
                process::exit(1);
            });

            println!("Secret text: {}\nViews left: {}", secret.text, secret.views);
        }
        None => {}
    }

    Ok(())
}
