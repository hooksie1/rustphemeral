use reqwest::Client;
use serde::{Serialize,Deserialize};
use std::process;

const GOPHEMERAL_URL: &str = "https://api.gophemeral.com/api/message";

#[derive(Serialize, Deserialize, Debug)]
pub struct Secret {
    pub text: String,
    pub views: u8,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SecretID {
    pub id: String,
    pub password: String,
}

// #[derive(Deserialize)]
// struct SecretError {
//     details: String,
// }

impl Secret {
    pub async fn new_secret(text: String, views: u8) -> Secret {
        Secret{
            text: text,
            views: views,
        }
    }
    pub async fn create(&self) -> Result<SecretID, reqwest::Error>{

        let resp = Client::new()
        .post(GOPHEMERAL_URL)
        .json(&self)
        .send()
        .await?;

        let data = resp
        .json::<SecretID>()
        .await.unwrap_or_else(|e| {
            println!("error unmarshaling json: {}", e);
            process::exit(1);
        });

        Ok(data)
    }
}

impl SecretID {
    pub async fn lookup(&self) -> Result<Secret, reqwest::Error>{
        let resp = Client::new()
        .get(GOPHEMERAL_URL)
        .header("X-Password", &self.password)
        .query(&[("id", &self.id)])
        .send()
        .await?;

        let data = resp
        .json::<Secret>()
        .await.unwrap_or_else(|e| {
            println!("error unmarshaling json: {}", e);
            process::exit(1);
        });

        Ok(data)
    }
}